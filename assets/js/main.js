function init() {
  var output = document.querySelector('main .content')
  get('guide/data.json', function(json) {
    var data = JSON.parse(json)
    each(data, function(section, section_name) {
      output.appendChild(el('h2', null, section.title))
      output.appendChild(el('section', {
        id: 'section-guide-' + section_name,
        source: 'guide/sections/' + section_name
      }))
      if (section.sections) {
        each(section.sections, function(subsection, subsection_name) {
          output.appendChild(el('h3', null, subsection.title))
          output.appendChild(el('article', {
            id: 'section-guide-' + section + '-' + subsection,
            source: 'guide/sections/' + section_name + '/' + subsection_name
          }))
        })
      }
    })
    get('reference/data.json', function(json) {
      var data = JSON.parse(json)
      output.appendChild(el('h2', null, 'Binder Reference'))
      each(data, function(section) {
        output.appendChild(el('h3', null, section))
        output.appendChild(el('section', {
          id: 'section-reference-' + section,
          source: 'reference/binders/' + section
        }))
      })
      getSources(function() {
        hljs.initHighlighting()
      })
    })
  })
}

function get(url, cb) {
  var xhr = new XMLHttpRequest()
  xhr.open('GET', url, true)
  xhr.onload = function() {
    if (xhr.status >= 200 && xhr.status < 400) {
      cb && cb(xhr.responseText)
    }
  }
  xhr.send()
}

function el(name, params, html) {
  var node = document.createElement(name)
  if (params) for (prop in params) node.setAttribute(prop, params[prop])
  if (html) node.innerHTML = html
  return node
}

function getSources(cb) {
  var nodes = document.querySelectorAll('[source]'),
    count = 0, length = nodes.length
  each(nodes, function(node, index) {
    var file = node.getAttribute('source')
    get(file + '.md', function(text) {
      node.innerHTML = marked(text)
      count++
      if (count === length) cb()
    })
  })
}

function each(source, iterator) {
  var i = 0
  if (source.length) {
    for (; i < source.length; i++) iterator(source[i], i)
  } else {
    for (i in source) iterator(source[i], i)
  }
}

init()