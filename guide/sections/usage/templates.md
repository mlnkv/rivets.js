Шаблоны описывают ваш пользовательский интерфейс (UI) при помощи обычного HTML. Вы можете их описыватьт прям в документе, используя тег `template` или хранить и загружать их как вам нравится. Главное - убедитесь что у вас есть удобный способ получить доступ к вашим шаблонам когда вам понадобится привязать какие-либо данные к ним.

```html
<section id="auction">

  <h3>{ auction.product.name }</h3>
  <p>Current bid: { auction.currentBid | money }</p>

  <aside rv-if="auction.timeLeft | lt 120">
    Hurry up! There is { auction.timeLeft | time } left.
  </aside>

</section>
```

Обратите внимание на аттрибуты начинающиеся с префикса `rv-` и тексчт обернутый в `{ ... }`. Это декларативный подход к связыванию данных в шаблонах, который использует Rivets.js. Такой способ описания значений имеет довольно лаконичный синтаксис.

```
(keypath | primitive) [formatters...]
```

`keypaths` описывает путь к данным за которыми надо наблюдать и пересчитывать связаные элементы всякий раз, когда эти значения изменятся. `primitive` может быть `string`, `number`, `boolean`, `null` или `undefined`.

[`formatters`](#formatters) can be piped to values using `|` and they follow a similarly minimal yet expressive syntax. [Formatter arguments](#formatters-arguments) can be keypaths or primitives. Keypath arguments get observed and will recompute the binding when any intermediary key changes.

```
(formatter) [keypath | primitive...]
```
