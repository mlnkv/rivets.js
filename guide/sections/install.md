Последний стабильный релиз вы можете взять [здесь](/dist/rivets.min.js) или установить его при помощи менеджера пакетов на ваш выбор. На данный момент поддерживаются npm, component, jsm и bower (рекомендуемо).

```bash
bower install rivets
```

Единственная зависимость Rivets - это [Sightglass](https://github.com/mikeric/sightglass). Если вы желаете подключить библиотеку Sightglass отдельно, убедитесь что она подключена первой.

```html
<script src="bower_components/sightglass/index.js"></script>
<script src="bower_components/rivets/dist/rivets.min.js"></script>
```

Как альтернатива, вы можете подключить сборку Rivets, которая включает в себя обе библиотеки.

```html
<script src="bower_components/rivets/dist/rivets.bundled.min.js"></script>
```

*Имейте ввиду, объект Rivets хранится в глобальной переменной `rivets`, но при этом поддерживаются загрузчики модулей CommonJS и AMD такие как [RequireJS](http://requirejs.org/) и [almond](https://github.com/jrburke/almond).*
